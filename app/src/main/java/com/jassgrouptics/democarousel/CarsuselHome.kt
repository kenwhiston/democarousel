package com.jassgrouptics.democarousel

import android.content.Context
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ViewListener

class CarsuselHome(context: Context, val callback: ()-> Unit): AlertDialog(context) {

    var dialogNotification: LinearLayout? = null
    var customCarouselView: CarouselView? = null
    var lista = arrayOf("TITULO 1","TITULO 2","TITULO 3")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ly_carusel_home)
        //this.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialogNotification = findViewById(R.id.dialogNotification)
        customCarouselView = findViewById(R.id.carouselView)

        setParentContainerAnimation()

        castView()

    }

    private fun castView(){
        customCarouselView!!.pageCount = lista.size
        val listener = ViewListener { position ->
            val customView = layoutInflater.inflate(R.layout.ly_item_anuncio, null)
            val img = customView.findViewById<ImageView>(R.id.img)
            val titulo = customView.findViewById<TextView>(R.id.titulo)
            val descripcion = customView.findViewById<TextView>(R.id.descripcion)
            val enlace = customView.findViewById<TextView>(R.id.enlace)

            titulo.text = lista[position]

            customView
        }

        customCarouselView!!.setViewListener(listener)
    }

    private fun setParentContainerAnimation() {
        dialogNotification!!.animation = ScaleAnimation(0f, 1f, 0f, 1f,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f)
            .apply { duration = 300 }
            .also { it.start() }
    }

}