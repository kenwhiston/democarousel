package com.jassgrouptics.democarousel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.jassgrouptics.democarousel.databinding.ActivityMainBinding
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ViewListener

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var customCarouselView: CarouselView? = null

    var lista = arrayOf("TITULO 1","TITULO 2","TITULO 3")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        castView()
    }

    private fun castView() {
        customCarouselView = binding.carouselView
        customCarouselView!!.pageCount = lista.size

        /*
        val listener = object : ViewListener {
            override fun setViewForPosition(position: Int): View {
                val customView = layoutInflater.inflate(R.layout.ly_item_anuncio, null)
                val img = customView.findViewById<ImageView>(R.id.img)
                val titulo = customView.findViewById<TextView>(R.id.titulo)
                val descripcion = customView.findViewById<TextView>(R.id.descripcion)
                val enlace = customView.findViewById<TextView>(R.id.enlace)

                titulo.text = lista[position]

                return customView
            }
        }
        */

        val listener = ViewListener { position ->
            val customView = layoutInflater.inflate(R.layout.ly_item_anuncio, null)
            val img = customView.findViewById<ImageView>(R.id.img)
            val titulo = customView.findViewById<TextView>(R.id.titulo)
            val descripcion = customView.findViewById<TextView>(R.id.descripcion)
            val enlace = customView.findViewById<TextView>(R.id.enlace)

            titulo.text = lista[position]

            customView
        }

        customCarouselView!!.setViewListener(listener)

        binding.btnCallCarusel.setOnClickListener {
            CarsuselHome(it!!.context) {

            }.show()
        }

        /*val listener = ViewListener {
            position ->
            val customView = layoutInflater.inflate(R.layout.ly_item_anuncio,null)
            return customView
        }*/
    }
}